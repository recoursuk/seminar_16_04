package TaskService;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestBuffer {
    @Test
    public void testBufferReturnedTasks(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(3);

        Task firstTask = new Task(1, 1, 1);
        Task secondTask = new Task(2, 2, 2);
        Task thirdTask = new Task(3, 3 ,3);

        simpleBuffer.add(firstTask);
        simpleBuffer.add(secondTask);
        simpleBuffer.add(thirdTask);

        Assert.assertEquals(firstTask, simpleBuffer.get());
        Assert.assertEquals(secondTask, simpleBuffer.get());
        Assert.assertEquals(thirdTask, simpleBuffer.get());
    }
}
