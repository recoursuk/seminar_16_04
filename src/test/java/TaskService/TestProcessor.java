package TaskService;

import org.junit.Assert;
import org.junit.Test;

public class TestProcessor {
    @Test
    public void testSimpleTaskProcessor(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(10);
        SimpleTaskGenerator simpleTaskGenerator = new SimpleTaskGenerator(simpleBuffer);
        SimpleTaskProcessor simpleTaskProcessor = new SimpleTaskProcessor(simpleBuffer);

        simpleTaskGenerator.withStartValue(1).withAmount(7).generate();
        simpleTaskGenerator.withStartValue(5).withAmount(4).generate();
        simpleTaskGenerator.withStartValue(0).withAmount(4).generate();

        Assert.assertEquals(Integer.valueOf(28), simpleTaskProcessor.process());
        Assert.assertEquals(Integer.valueOf(26), simpleTaskProcessor.process());
        Assert.assertEquals(Integer.valueOf(6), simpleTaskProcessor.process());
        Assert.assertNull(simpleTaskProcessor.process());
    }
}
