package TaskService;

import java.util.Arrays;

public class SimpleTaskProcessor implements ITaskProcessor {
    private SimpleBuffer simpleBuffer;

    public SimpleTaskProcessor(SimpleBuffer simpleBuffer){
        this.simpleBuffer = simpleBuffer;
    }

    @Override
    public Integer process() {
        if (simpleBuffer.isEmpty()) {
            return null;
        }
        return Arrays.stream(simpleBuffer.get().getData()).sum();
    }
}
