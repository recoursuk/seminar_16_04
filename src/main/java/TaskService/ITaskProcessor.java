package TaskService;

public interface ITaskProcessor {
    Integer process();
}
