package TaskService;

public class PriorityTask extends Task implements ITask, Comparable<PriorityTask> {
    private int priority;

    public PriorityTask(int priority, Integer... data){
        super(data);
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PriorityTask that = (PriorityTask) o;

        return priority == that.priority;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + priority;
        return result;
    }


    @Override
    public int compareTo(PriorityTask o) {
        return Integer.compare(this.priority, o.priority);
    }
}
