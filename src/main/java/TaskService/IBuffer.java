package TaskService;

public interface IBuffer <T extends Task>{
    void add(T elem);
    T get();
    int size();
    void clear();
    boolean isEmpty();
}
